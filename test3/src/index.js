import _ from 'lodash';
import printMe from './print.js'
// import './style.css';
// import MyImage from './timg.jpeg';
// import Data from './data.xml';

function component() {
    var element = document.createElement('div');
    var btn = document.createElement('button');
    // Lodash（目前通过一个 script 脚本引入）对于执行这一行是必需的
    element.innerHTML = _.join(['Hello', 'wp'], ' ');
    btn.innerHTML = 'Click me and check the console!';
    btn.onclick = printMe;
    element.appendChild(btn);
    // element.classList.add('hello');
    // var myIcon = new Image();
    // myIcon.src = MyImage;
    // element.appendChild(myIcon);
    // console.log(Data);
    return element;
  }
  
  document.body.appendChild(component());