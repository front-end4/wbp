const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const  {CleanWebpackPlugin} = require('clean-webpack-plugin');
console.log(process.argv);
module.exports={

    entry:{
        app1:'./src/index.js',
        print1:'./src/print.js'
    },
    output:{
        
        filename:'[name].bundle.js',
        path:path.resolve(__dirname,'dist'),
        publicPath: '/'
    },
    plugins:[
        // new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title:'output management'
        })
    ],
    // devServer:{
    //     contentBase:'./dist'
    // },
    //watch:true,

    devtool: 'inline-source-map'
    // module:{
    //     rules:[
    //         {
    //             test:/\.css$/,
    //             use:[
    //                 'style-loader',
    //                 'css-loader'
    //             ]
    //         },
    //         {
    //             test:/\.(png|svg|jpg|gif|jpeg)$/,
    //             use:[
    //                 'file-loader'
                   
    //             ]
    //         },
    //         {
    //             test: /\.(woff|woff2|eot|ttf|otf)$/,
    //             use: [
    //                   'file-loader'
    //             ]
    //         },
    //         {
    //             test:/\.xml$/,
    //             use:['xml-loader']
    //         }
    //     ]
    // }
}