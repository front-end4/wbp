const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const  {CleanWebpackPlugin} = require('clean-webpack-plugin');
module.exports={
    mode: 'development',

    // entry:{
    //     app1:'./src/index.js',
    // },
    entry:[

        'webpack-hot-middleware/client?path=http://localhost:3000/__webpack_hmr',
        './src/index.js'
    ],
    output:{
        
        filename: 'bundle.js',
        path: __dirname,
        publicPath: '/'
    },
    plugins:[
        // new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title:'output management'
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
    ],
    // devServer:{
    //     contentBase:'./dist'
    // },
    //watch:true,

    devtool: 'inline-source-map'
    // module:{
    //     rules:[
    //         {
    //             test:/\.css$/,
    //             use:[
    //                 'style-loader',
    //                 'css-loader'
    //             ]
    //         },
    //         {
    //             test:/\.(png|svg|jpg|gif|jpeg)$/,
    //             use:[
    //                 'file-loader'
                   
    //             ]
    //         },
    //         {
    //             test: /\.(woff|woff2|eot|ttf|otf)$/,
    //             use: [
    //                   'file-loader'
    //             ]
    //         },
    //         {
    //             test:/\.xml$/,
    //             use:['xml-loader']
    //         }
    //     ]
    // }
}