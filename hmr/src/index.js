import _ from 'lodash';
import printMe from './print.js'
import test from './test.js'
// import './style.css';
// import MyImage from './timg.jpeg';
// import Data from './data.xml';

function component() {
    var element = document.createElement('div');
    var btn = document.createElement('button');
    var btn2 = document.createElement('button');
    // Lodash（目前通过一个 script 脚本引入）对于执行这一行是必需的
    element.innerHTML = _.join(['Hello', 'wp'], ' ');
    btn.innerHTML = 'Click me and check the console!';
    btn.onclick = printMe;
    btn2.onclick=test;
    element.appendChild(btn);
    element.appendChild(btn2)
    element.id="aj";
    // element.classList.add('hello');
    // var myIcon = new Image();
    // myIcon.src = MyImage;
    // element.appendChild(myIcon);
    // console.log(Data);
    return element;
  }

    let aj =document.getElementById("aj");
    if(aj){
        document.body.removeChild(aj);
    }
    let element = component(); // 当 print.js 改变导致页面重新渲染时，重新获取渲染的元素
    document.body.appendChild(element);

if (module.hot) {
    module.hot.accept();
    // console.log(element);
    // document.body.removeChild(element);
    // element = component(); // 重新渲染页面后，component 更新 click 事件处理
    // document.body.appendChild(element);

}